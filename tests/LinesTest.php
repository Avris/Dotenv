<?php

namespace Avris\Dotenv;

use Avris\Dotenv\Line\IgnoredLine;
use Avris\Dotenv\Line\InvalidLine;
use Avris\Dotenv\Line\Line;
use Avris\Dotenv\Line\QuestionLine;
use Avris\Dotenv\Line\SectionEndLine;
use Avris\Dotenv\Line\SectionStartLine;
use Avris\Dotenv\Line\VarLine;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Dotenv\Line\Line
 * @covers \Avris\Dotenv\Line\VarLine
 * @covers \Avris\Dotenv\Line\SectionStartLine
 * @covers \Avris\Dotenv\Line\SectionEndLine
 * @covers \Avris\Dotenv\Line\IgnoredLine
 * @covers \Avris\Dotenv\Line\InvalidLine
 * @covers \Avris\Dotenv\Line\QuestionLine
 */
class LinesTest extends TestCase
{
    public function testLines()
    {
        $lines = [
            new SectionStartLine('SEC', 0),
            new VarLine('VAR1', 'value', 1),
            new VarLine('VAR2', 'spaced" value', 2),
            new SectionEndLine('SEC', 3),
            new IgnoredLine('#comment', 4),
            new InvalidLine('osiem', 5),
        ];

        $expected = <<<ENV
###> SEC ###
VAR1=value
VAR2="spaced\" value"
###< SEC ###
#comment
osiem

ENV;

        $this->assertEquals(
            $expected,
            join('', $lines)
        );

        $numbers = array_map(function (Line $line) {
            return $line->getNumber();
        }, $lines);

        $this->assertEquals(range(0, 5), $numbers);
    }

    public function testQuestionLine()
    {
        $question = new QuestionLine('SRSLY', 'yes');

        $this->assertEquals('yes', $question->getDefault());
        $this->assertEquals(-1, $question->getNumber());

        $question->answer('hell yeah!');

        $this->assertEquals('SRSLY="hell yeah!"' . PHP_EOL, (string) $question);

        $question->answer('nope');

        $this->assertEquals('SRSLY=nope' . PHP_EOL, (string) $question);
    }
}
