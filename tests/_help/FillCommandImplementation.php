<?php

final class FillCommandImplementation extends \Avris\Dotenv\Command\FillCommand
{
    /** @var array */
    private $defaults;

    public function __construct(array $defaults)
    {
        $this->defaults = $defaults;
        parent::__construct();
    }

    protected function getDefaults(): iterable
    {
        return new \ArrayIterator($this->defaults);
    }
}
