<?php

namespace Avris\Dotenv\Command;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @covers \Avris\Dotenv\Command\FillCommand
 */
class FillCommandTest extends TestCase
{
    const DIR = __DIR__ . '/../_help/';
    const FILE_TEMPLATE = self::DIR . 'command.template.env';
    const FILE_WORK = self::DIR . 'command.env';
    const FILE_EXPECTED = self::DIR . 'command.expected.env';

    public function testRun()
    {
        $expected = [
            '' => [
                'FOO' => 'foo',
                'BAR' => 'bar',
                'SECRET' => '%generate(secret)%',
            ],
            'Module' => [
                'MOD_FOO' => 'modfoo',
                'MOD_BAR' => 'modbar',
            ],
            'Another' => [
                'AN_FOO' => 'anfoo',
            ],
        ];

        $app = new Application();
        $app->add(new \FillCommandImplementation($expected));

        $command = $app->find('dotenv:fill');

        copy(self::FILE_TEMPLATE, self::FILE_WORK);

        $commandTester = new CommandTester($command);
        $commandTester->setInputs(['m', 'space words', 'ok']);
        $commandTester->execute([
            'command' => $command->getName(),
            'file' => self::FILE_WORK,
        ]);

        $this->assertEquals(
            'MOD_BAR (modbar): BAR (bar): AN_FOO (anfoo): ',
            $commandTester->getDisplay()
        );

        $this->assertEquals(
            file_get_contents(self::FILE_EXPECTED),
            preg_replace('#^SECRET=[A-Za-z0-9]{64}$#m', 'SECRET=--SECRET--', file_get_contents(self::FILE_WORK))
        );
    }

    private function getInputStream($input)
    {
        $stream = fopen('php://memory', 'r+', false);
        fputs($stream, $input);
        rewind($stream);

        return $stream;
    }
}
