<?php

namespace Avris\Dotenv;

use Avris\Dotenv\Dotenv;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Dotenv\Dotenv
 */
class DotenvTest extends TestCase
{
    const FILE = __DIR__ . '/_help/simple.env';
    const OUTPUT_FILE = __DIR__ . '/_help/output.env';

    const VARS_GROUPED = [
        '' => [ 'FOO' => 'foo' ],
        'Avris\Micrus' => [ 'BAR' => 'bar' ],
    ];

    const VARS = [
        'FOO' => 'foo',
        'BAR' => 'bar',
    ];

    /** @var Dotenv */
    private $dotenv;

    protected function setUp()
    {
        $this->dotenv = new Dotenv();

        unset($_ENV['FOO']);
        unset($_ENV['BAR']);
    }

    public function testLoad()
    {
        $result = $this->dotenv->load(self::FILE);
        $this->assertEquals(self::VARS, $result);

        $this->assertSame('foo', $_ENV['FOO']);
        $this->assertSame('bar', $_ENV['BAR']);
    }

    public function testRead()
    {
        $result = $this->dotenv->read(self::FILE);
        $this->assertEquals(self::VARS_GROUPED, $result);

        $this->assertFalse(isset($_ENV['FOO']));
        $this->assertFalse(isset($_ENV['BAR']));
    }

    public function testParse()
    {
        $result = $this->dotenv->parse(file_get_contents(self::FILE));
        $this->assertEquals(self::VARS_GROUPED, $result);

        $this->assertFalse(isset($_ENV['FOO']));
        $this->assertFalse(isset($_ENV['BAR']));
    }

    public function testPopulate()
    {
        $result = $this->dotenv->populate(self::VARS_GROUPED);
        $this->assertEquals(self::VARS, $result);

        $this->assertSame('foo', $_ENV['FOO']);
        $this->assertSame('bar', $_ENV['BAR']);
    }

    public function testFill()
    {
        $result = iterator_to_array($this->dotenv->fill(self::FILE, new \ArrayIterator(self::VARS_GROUPED)));
        $this->assertEquals(
            file_get_contents(self::FILE) . PHP_EOL,
            join('', $result)
        );

        $this->assertFalse(isset($_ENV['FOO']));
        $this->assertFalse(isset($_ENV['BAR']));
    }

    public function testDump()
    {
        $result = $this->dotenv->dump([
            'FOO' => 'foo',
            'Avris\Micrus' => [
                'BAR' => 'bar',
            ],
        ]);
        $this->assertEquals(
            file_get_contents(self::FILE) . PHP_EOL,
            $result
        );

        $this->assertFalse(isset($_ENV['FOO']));
        $this->assertFalse(isset($_ENV['BAR']));
    }

    public function testSaveString()
    {
        if (file_exists(self::OUTPUT_FILE)) {
            unlink(self::OUTPUT_FILE);
        }

        $result = $this->dotenv->save(self::OUTPUT_FILE, 'FOO=foo' . PHP_EOL);
        $this->assertEquals('FOO=foo' . PHP_EOL, $result);
        $this->assertEquals('FOO=foo' . PHP_EOL, file_get_contents(self::OUTPUT_FILE));

        $this->assertFalse(isset($_ENV['FOO']));
        $this->assertFalse(isset($_ENV['BAR']));
    }

    public function testSaveArray()
    {
        if (file_exists(self::OUTPUT_FILE)) {
            unlink(self::OUTPUT_FILE);
        }

        $result = $this->dotenv->save(self::OUTPUT_FILE, ['FOO' => 'foo']);
        $this->assertEquals('FOO=foo' . PHP_EOL, $result);
        $this->assertEquals('FOO=foo' . PHP_EOL, file_get_contents(self::OUTPUT_FILE));

        $this->assertFalse(isset($_ENV['FOO']));
        $this->assertFalse(isset($_ENV['BAR']));
    }
}
