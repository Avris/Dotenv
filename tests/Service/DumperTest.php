<?php

namespace Avris\Dotenv\Service;

use Avris\Dotenv\Service\Dumper;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Dotenv\Service\Dumper
 */
class DumperTest extends TestCase
{
    public function testDump()
    {
        $dumper = new Dumper();

        $variables = [
            'FOO' => 'foo',
            'Avris\Test' => [
                'BAR' => 'bar',
            ],
            'Avris\Lol' => [
                'LOL' => 'lol',
                'OK' => 'ok',
            ],
        ];

        $expected = <<<ENV
FOO=foo
###> Avris\Test ###
BAR=bar
###< Avris\Test ###

###> Avris\Lol ###
LOL=lol
OK=ok
###< Avris\Lol ###


ENV;

        $this->assertEquals($expected, $dumper->dump($variables));
    }
}
