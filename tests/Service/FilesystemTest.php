<?php

namespace Avris\Dotenv\Service;

use PHPUnit\Framework\TestCase;

function file_get_contents()
{
    if (FilesystemTest::$mockfail) {
        return false;
    }

    return call_user_func_array('\file_get_contents', func_get_args());
}

function file_put_contents()
{
    if (FilesystemTest::$mockfail) {
        return false;
    }

    return call_user_func_array('\file_put_contents', func_get_args());
}

/**
 * @covers \Avris\Dotenv\Service\Filesystem
 */
class FilesystemTest extends TestCase
{
    const DIR = __DIR__ . '/../_help/fs/';

    public static $mockfail = false;

    /** @var Filesystem */
    private $filesystem;

    protected function setUp()
    {
        $this->filesystem = new Filesystem();
        self::$mockfail = false;
    }

    protected function tearDown()
    {
        self::$mockfail = false;
    }

    public function testReadFileExisting()
    {
        $this->assertEquals(
            'Content' . PHP_EOL,
            $this->filesystem->readFile(self::DIR . 'read.txt')
        );
    }

    public function testReadFileNotExisting()
    {
        $this->assertEquals(
            '',
            $this->filesystem->readFile(self::DIR . 'nonexistent.txt')
        );
    }

    /**
     * @expectedException \Avris\Dotenv\Exception\FilesystemException
     * @expectedExceptionMessage Cannot read from file
     */
    public function testReadFileFailed()
    {
        self::$mockfail = true;

        $this->filesystem->readFile(self::DIR . 'read.txt');
    }

    public function testSaveFile()
    {
        $path = self::DIR . 'dir/write.txt';

        if (file_exists($path)) {
            unlink($path);
        }

        if (file_exists(self::DIR . 'dir')) {
            rmdir(self::DIR . 'dir');
        }

        $this->filesystem->saveFile($path, 'osiem');

        $this->assertEquals('osiem', file_get_contents($path));
    }

    /**
     * @expectedException \Avris\Dotenv\Exception\FilesystemException
     * @expectedExceptionMessage read.txt is not a directory
     */
    public function testSaveFileWrongDir()
    {
        $this->filesystem->saveFile(self::DIR . 'read.txt/file.txt', 'osiem');
    }

    /**
     * @expectedException \Avris\Dotenv\Exception\FilesystemException
     * @expectedExceptionMessage Cannot write to file
     */
    public function testSaveFileFail()
    {
        self::$mockfail = true;

        $this->filesystem->saveFile(self::DIR . 'write.txt', 'osiem');
    }
}
