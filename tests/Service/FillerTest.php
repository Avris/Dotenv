<?php

namespace Avris\Dotenv\Service;

use Avris\Dotenv\Line\IgnoredLine;
use Avris\Dotenv\Line\QuestionLine;
use Avris\Dotenv\Line\SectionEndLine;
use Avris\Dotenv\Line\SectionStartLine;
use Avris\Dotenv\Line\VarLine;
use Avris\Dotenv\Service\Filler;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Dotenv\Service\Filler
 */
class FillerTest extends TestCase
{
    public function testFill()
    {
        $filler = new Filler();

        $lines = [
            new IgnoredLine('#comment'),
            new VarLine('FOO', 'ffoooo'),
            new VarLine('UNEXP', 'unexpected'),

            new SectionStartLine('Module'),
            new VarLine('MOD_FOO', 'mmooddffoooo'),
            new SectionEndLine('Module'),

            new SectionStartLine('Unexpected'),
            new VarLine('MOD_UNEXP', 'mod_unexpected'),
            new SectionEndLine('Unexpected'),
        ];

        $expected = [
            '' => [
                'FOO' => 'foo',
                'BAR' => 'bar',
            ],
            'Module' => [
                'MOD_FOO' => 'modfoo',
                'MOD_BAR' => 'modbar',
            ],
            'Another' => [
                'AN_FOO' => 'anfoo',
            ],
        ];

        $result = iterator_to_array($filler->fill($lines, new \ArrayIterator($expected)));

        $expected = [
            new IgnoredLine('#comment'),
            new VarLine('FOO', 'ffoooo'),
            new VarLine('UNEXP', 'unexpected'),

            new SectionStartLine('Module'),
            new VarLine('MOD_FOO', 'mmooddffoooo'),
            new QuestionLine('MOD_BAR', 'modbar'),
            new SectionEndLine('Module'),

            new SectionStartLine('Unexpected'),
            new VarLine('MOD_UNEXP', 'mod_unexpected'),
            new SectionEndLine('Unexpected'),

            new QuestionLine('BAR', 'bar'),

            new SectionStartLine('Another'),
            new QuestionLine('AN_FOO', 'anfoo'),
            new SectionEndLine('Another'),
        ];

        $this->assertEquals($expected, $result);

        $answers = [
            'MOD_BAR' => 'm',
            'BAR' => 'space words',
            'AN_FOO' => 'ok',
        ];

        foreach ($result as $line) {
            if ($line instanceof QuestionLine) {
                $line->answer($answers[$line->getName()]);
            }
        }

        $expectedString = <<<ENV
#comment
FOO=ffoooo
UNEXP=unexpected
###> Module ###
MOD_FOO=mmooddffoooo
MOD_BAR=m
###< Module ###
###> Unexpected ###
MOD_UNEXP=mod_unexpected
###< Unexpected ###
BAR="space words"
###> Another ###
AN_FOO=ok
###< Another ###

ENV;

        $this->assertEquals($expectedString, join('', $result));
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Invalid format of the $expected array
     */
    public function testFillInvalid()
    {
        $filler = new Filler();

        iterator_to_array($filler->fill([], new \ArrayIterator(['foo' => 'Invalid'])));
    }
}
