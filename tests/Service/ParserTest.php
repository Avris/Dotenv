<?php

namespace Avris\Dotenv\Service;

use Avris\Dotenv\Service\Dumper;
use Avris\Dotenv\Service\Parser;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Dotenv\Service\Parser
 * @covers \Avris\Dotenv\Exception\ParseException
 */
class ParserTest extends TestCase
{
    /** @var Parser */
    private $parser;

    protected function setUp()
    {
        $this->parser = new Parser();
    }

    public function testParse()
    {
        $expected = [
            '' => [
                'FOO' => 'abec"adło',
                'BAR' => 'ba"r',
                'VAR1' => 'osiem',
                'VAR2' => 'osiemnaście lat',
                'VAR3' => '$ESC',
                'COMM1' => get_current_user(),
                'COMM2' => '$(whoami)',
            ],
            'Avris\Micrus' => [
                'APP_ENV' => 'dev',
                'APP_DEBUG' => '1',
                'APP_SECRET' => 'edcbbf8b01a652e0a8b0a59e150177061a317e8c07030b15064a65a5a2637161',
                'URL_SCHEME' => 'http',
                'URL_HOST' => 'localhost',
                'URL_BASE' => '',
            ],
            'Avris\Micrus\Doctrine' => [
                'DB_DRIVER' => 'pdo_mysql',
                'DB_HOST' => '127.0.0.1',
                'DB_NAME' => 'micrus',
                'DB_USER' => 'root',
                'DB_PASS' => '',
            ],
            'Avris\Micrus\Mailer' => [
                'MAILER_SMTP' => '1',
                'MAILER_HOST' => 'localhost',
                'MAILER_PORT' => '25',
                'MAILER_SECURE' => '',
                'MAILER_USERNAME' => 'root',
                'MAILER_PASSWORD' => '',
            ],
            'Avris\Micrus\GoogleAnalytics' => [
                'GA_TRACKING_ID' => 'GOOGLE_ANALYTICS_TRACKING_ID',
            ],
        ];

        putenv('CZEGO=lat');
        $vars = $this->parser->parse($this->readTestCase('good'));

        $this->assertEquals($expected, $vars);
    }

    /**
     * @expectedException \Avris\Dotenv\Exception\ParseException
     * @expectedExceptionMessage Parsing error in line 3
     */
    public function testParseWrongSectionOpen()
    {
        $this->parser->parse($this->readTestCase('wrongSectionOpen'));
    }

    /**
     * @expectedException \Avris\Dotenv\Exception\ParseException
     * @expectedExceptionMessage Parsing error in line 2
     */
    public function testParseWrongSectionClose()
    {
        $this->parser->parse($this->readTestCase('wrongSectionClose'));
    }

    /**
     * @expectedException \Avris\Dotenv\Exception\ParseException
     * @expectedExceptionMessage Parsing error in line 4
     */
    public function testParseWrongLine()
    {
        $this->parser->parse($this->readTestCase('wrongLine'));
    }

    /**
     * @expectedException \Avris\Dotenv\Exception\ParseException
     * @expectedExceptionMessage Parsing error in line 0
     */
    public function testParseWrongQuotes1()
    {
        $this->parser->parse($this->readTestCase('wrongQuotes1'));
    }

    /**
     * @expectedException \Avris\Dotenv\Exception\ParseException
     * @expectedExceptionMessage Parsing error in line 0
     */
    public function testParseWrongQuotes2()
    {
        $this->parser->parse($this->readTestCase('wrongQuotes2'));
    }

    /**
     * @expectedException \Avris\Dotenv\Exception\ParseException
     * @expectedExceptionMessage Parsing error in line 0
     */
    public function testParseWrongVar()
    {
        $this->parser->parse($this->readTestCase('wrongVar'));
    }

    private function readTestCase(string $name): string
    {
        return file_get_contents(__DIR__ . '/../_help/' . $name . '.env');
    }
}
