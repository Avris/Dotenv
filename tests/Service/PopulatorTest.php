<?php

namespace Avris\Dotenv\Service;

use Avris\Dotenv\Service\Populator;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Dotenv\Service\Populator
 */
class PopulatorTest extends TestCase
{
    public function testPopulate()
    {
        $populator = new Populator();

        $this->put('POP_EXISTING', 'existing');
        $this->put('HTTP_POP', 'http');

        $populator->populate([
            'POP_FOO' => 'foo',
            'module' => [
                'POP_BAR' => 'bar',
            ],
            'POP_EXISTING' => 'overwritten',
            'HTTP_POP' => 'overwritten',
            'HTTP_NEW' => 'new',
        ]);

        $this->assertEnv('POP_FOO', 'foo');
        $this->assertEnv('POP_BAR', 'bar');
        $this->assertEnv('POP_EXISTING', 'existing');
        $this->assertEnv('HTTP_POP', 'http');
        $this->assertEnv('HTTP_NEW', 'new', false);

        $populator->populate([
            'POP_FOO' => 'overwritten',
        ]);

        $this->assertEnv('POP_FOO', 'overwritten');
    }

    private function put(string $name, string $value)
    {
        putenv("$name=$value");
        $_ENV[$name] = $value;
        $_SERVER[$name] = $value;
    }

    private function assertEnv(string $name, string $value, $server = true)
    {
        $this->assertEquals($value, getenv($name));
        $this->assertEquals($value, $_ENV[$name]);
        if ($server) {
            $this->assertEquals($value, $_SERVER[$name]);
        } else {
            $this->assertFalse(isset($_SERVER[$name]));
        }
    }
}
