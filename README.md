## Avris Dotenv ##

### Instalation

    composer require avris/dotenv
    
### .env

Environmental variables are a great way to adjust your application to a specific environment,
especially when it comes to sensitive data, like database password.

Those variables should be set by your HTTP server,
but for local development having a `.env` file is way simpler:

    APP_ENV=prod
    APP_DEBUG=0
    
    DB_HOST=127.0.0.1
    DB_DATABASE=prod
    DB_USER=user
    DB_PASS=pass
    
All you need to do in your front controller is load this file using Dotenv:

    (new \Avris\Dotenv\Dotenv)->load(__DIR__ . '/../.env');
    
Now all the variables from the file will be available via `getenv`, `$_ENV` and `$_SERVER`.

### Advanced

If your variable value contains a space, remember to quote it:

    FOO="Lorem ipsum"
    
You can add a comment if you start a line with a `#`:

    # comment
    FOO=bar
    
You can use vars inside other vars (escape `$` with `\$`):

    VAR1=osiem
    VAR2="${VAR1}naście ${CZEGO}"
    VAR3=\$ESC

You can also use output of a command (`symfony/process` is required, escape `$` with `\$`):

    COMM1=$(whoami)
    COMM2=\$(whoami) 

Remember .env is a valid bash script.

### Other features

You can just read the variables from a file without populating it as environment variables: 

    $vars = (new \Avris\Dotenv\Dotenv)->read(__DIR__ . '/../.env');

Or just populate vars into environment without parsing a file:

    (new \Avris\Dotenv\Dotenv)->populate([
        'FOO' => 'foo',
        'BAR' => 'bar',
    ]);
    
You can also rever the process and dump vars into a .env file:

    (new \Avris\Dotenv\Dotenv)->save(__DIR__ . '/.env', [
        'FOO' => 'foo',
        'BAR' => 'bar',
    ]);
    
Or without saving to a file:

    $string = (new \Avris\Dotenv\Dotenv)->dump([
        'FOO' => 'foo',
        'BAR' => 'bar',
    ]);

### Fill command

Dotenv provides a simple way to fill your `.env` file with values based on user input and defaults.

Let's say you have an application with two modules that require some environment variables: `Foo\Database` and `Foo\Mailer`.
Install `symfony/console` and create a command that extends `Avris\Dotenv\Command\FillCommand`, providing all the defaults:

    final class FillCommandImplementation extends \Avris\Dotenv\Command\FillCommand
    {
        protected function getDefaults(): iterable
        {
            yield `Foo\Database` => [
                'DB_HOST' => '127.0.0.1',
                'DB_DATABASE' => 'foo',
                'DB_USER' => 'root',
                'DB_PASS' => '',
            ]; 
            yield `Foo\Mailer` => [
                'MAILER_URL' => 'null://localhost',
            ];
        }
    }

Let's say your `.env` file looks like this:

    ###> Foo\Database ###
    DB_HOST=127.0.0.1
    DB_DATABASE=prod
    DB_USER=user
    ###< Foo\Database ###
    
    FOO=bar
    
Both `DB_PASS` and `MAILER_URL` are missing and an additional variable `FOO` is defined.

If you now [run the command](https://symfony.com/doc/current/components/console.html),
you will be asked what should `DB_PASS` and `MAILER_URL` be (defaults `''` and `null://localhost` are offered),
and the values you submit will be put in the `.env` file without removing `FOO`.  

### Copyright ###

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
