<?php
namespace Avris\Dotenv\Command;

use Avris\Dotenv\Dotenv;
use Avris\Dotenv\Line\QuestionLine;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

abstract class FillCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('dotenv:fill')
            ->setDescription('Creates a .env file based on user input and provided defaults')
            ->addArgument('file', null, '.env file to fill with data', '.env')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dotenv = new Dotenv();
        $filename = $input->getArgument('file');

        $newLines = [];
        foreach ($dotenv->fill($filename, $this->getDefaults()) as $line) {
            if ($line instanceof QuestionLine) {
                $line->answer($this->answerQuestion($line, $input, $output));
            }
            $newLines[] = $line;
        }

        $dotenv->save(
            $filename, // @codeCoverageIgnore
            $this->separateSections(implode('', $newLines))
        );
    }

    abstract protected function getDefaults(): iterable;

    private function answerQuestion(QuestionLine $line, InputInterface $input, OutputInterface $output)
    {
        switch ($default = $line->getDefault()) {
            case '%generate(secret)%':
                return bin2hex(random_bytes(32));
            default:
                return $this->getHelper('question')->ask(
                    $input, // @codeCoverageIgnore
                    $output, // @codeCoverageIgnore
                    new Question(sprintf('%s (<info>%s</info>): ', $line->getName(), $default), $default)
                );
        }
    }

    private function separateSections(string $string)
    {
        return preg_replace('/ ###\s*###>/ ', " ###\n\n###>", $string);
    }
}
