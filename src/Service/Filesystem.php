<?php
namespace Avris\Dotenv\Service;

use Avris\Dotenv\Exception\FilesystemException;

final class Filesystem
{
    public function readFile(string $filename): string
    {
        if (!file_exists($filename)) {
            return '';
        }

        $content = file_get_contents($filename);
        if ($content === false) {
            throw new FilesystemException(sprintf('Cannot read from file %s', $filename));
        }

        return $content;
    }

    public function saveFile(string $filename, string $content): string
    {
        $dir = dirname($filename);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        if (!is_dir($dir)) {
            throw new FilesystemException(sprintf('Cannot write to file %s: %s is not a directory', $filename, $dir));
        }

        if (file_put_contents($filename, $content) === false) {
            throw new FilesystemException(sprintf('Cannot write to file %s', $filename));
        }

        return $content;
    }
}
