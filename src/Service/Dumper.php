<?php
namespace Avris\Dotenv\Service;

use Avris\Dotenv\Line\IgnoredLine;
use Avris\Dotenv\Line\SectionEndLine;
use Avris\Dotenv\Line\SectionStartLine;
use Avris\Dotenv\Line\VarLine;

final class Dumper
{
    public function dump(iterable $vars): string
    {
        return implode('', iterator_to_array($this->buildLines($vars)));
    }

    public function buildLines(iterable $vars): iterable
    {
        foreach ($vars as $name => $value) {
            if (is_iterable($value)) {
                yield new SectionStartLine($name);
                foreach ($value as $varName => $varValue) {
                    yield new VarLine($varName, $varValue);
                }
                yield new SectionEndLine($name);
                yield new IgnoredLine('');
            } else {
                yield new VarLine($name, $value);
            }
        }
    }
}
