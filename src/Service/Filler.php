<?php
namespace Avris\Dotenv\Service;

use Avris\Dotenv\Line\IgnoredLine;
use Avris\Dotenv\Line\QuestionLine;
use Avris\Dotenv\Line\SectionEndLine;
use Avris\Dotenv\Line\SectionStartLine;
use Avris\Dotenv\Line\VarLine;

final class Filler
{
    public function fill(iterable $lines, iterable $expected): iterable
    {
        $section = '';

        $expected = iterator_to_array($expected);

        foreach ($lines as $line) {
            if ($line instanceof IgnoredLine) {
                yield $line;
            } elseif ($line instanceof VarLine) {
                yield $line;
                unset($expected[$section][$line->getName()]);
            } elseif ($line instanceof SectionStartLine) {
                $section = $line->getSection();
                yield $line;
            } elseif ($line instanceof SectionEndLine) {
                foreach ($expected[$section] ?? [] as $name => $default) {
                    yield new QuestionLine($name, $default);
                    unset($expected[$section][$name]);
                }
                yield $line;
                unset($expected[$section]);
                $section = '';
            }
        }

        foreach ($expected as $section => $vars) {
            if (!is_iterable($vars)) {
                throw new \InvalidArgumentException('Invalid format of the $expected array');
            }

            if ($section !== '') {
                yield new SectionStartLine($section);
            }

            foreach ($vars as $name => $default) {
                yield new QuestionLine($name, $default);
                unset($expected[$section][$name]);
            }

            if ($section !== '') {
                yield new SectionEndLine($section);
            }
            unset($expected[$section]);
        }
    }
}
