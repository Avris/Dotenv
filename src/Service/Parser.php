<?php
namespace Avris\Dotenv\Service;

use Avris\Dotenv\Exception\ParseException;
use Avris\Dotenv\Line\IgnoredLine;
use Avris\Dotenv\Line\InvalidLine;
use Avris\Dotenv\Line\Line;
use Avris\Dotenv\Line\SectionEndLine;
use Avris\Dotenv\Line\SectionStartLine;
use Avris\Dotenv\Line\VarLine;
use Symfony\Component\Process\Process;

final class Parser
{
    const SECTION_NAME = '[A-Za-z0-9\\\\]+';
    const VAR_NAME = '[A-Z][A-Z0-9_]*';

    /** @var string[] */
    private $parsedValues = [];

    public function parse(string $data): array
    {
        $vars = [];
        $section = '';

        foreach ($this->getLines($data) as $line) {
            if ($line instanceof SectionStartLine) {
                if ($section !== '') {
                    throw new ParseException($line);
                }
                $section = $line->getSection();
            } elseif ($line instanceof SectionEndLine) {
                if ($section !== $line->getSection()) {
                    throw new ParseException($line);
                }
                $section = '';
            } elseif ($line instanceof VarLine) {
                $vars[$section][$line->getName()] = $line->getValue();
            } elseif ($line instanceof InvalidLine) {
                throw new ParseException($line);
            }
        }

        return $vars;
    }

    public function getLines(string $data): iterable
    {
        foreach (explode("\n", strtr($data, ["\r\n" => "\n"])) as $lineNr => $line) {
            yield $this->parseLine($lineNr, $line);
        }
    }

    private function parseLine(int $lineNr, string $line): Line
    {
        if (trim($line) === '') {
            return new IgnoredLine($line, $lineNr);
        }

        if (preg_match('/###> (' . self::SECTION_NAME . ') ###/Uui', $line, $matches)) {
            return new SectionStartLine($matches[1], $lineNr);
        }

        if (preg_match('/###< (' . self::SECTION_NAME . ') ###/Uui', $line, $matches)) {
            return new SectionEndLine($matches[1], $lineNr);
        }

        if (substr($line, 0, 1) === '#') {
            return new IgnoredLine($line, $lineNr);
        }

        if (!preg_match('/(' . self::SECTION_NAME . ')=(.*)/Uui', $line, $matches)) {
            return new InvalidLine($line, $lineNr);
        }

        list($name, $value) = explode('=', $line, 2);

        try {
            $value = $this->handleQuotes($value);
            $value = $this->handleVars($value);
            $value = $this->handleCommands($value);
            $value = strtr($value, ['\\$' => '$', '\\"' => '"']);
        } catch (\InvalidArgumentException $e) {
            return new InvalidLine($line, $lineNr);
        }

        $this->parsedValues[$name] = $value;

        return new VarLine($name, $value, $lineNr);
    }

    private function handleQuotes(string $value): string
    {
        if (preg_match('/^"(.*)"$/', $value, $matches)) {
            return $matches[1];
        }

        if (strpos($value, ' ') !== false || preg_match('#[^\\\\]"#', $value)) {
            throw new \InvalidArgumentException;
        }

        return $value;
    }

    private function handleVars(string $value): string
    {
        return preg_replace_callback('#
          (\\\\)?
          \$
          (\{)?
          (' . self::VAR_NAME . ')
          (\})?
        #x', function ($matches) {
            if ($matches[1] === '\\') {
                return substr($matches[0], 1);
            }

            if ($matches[2] === '{' && !isset($matches[4])) {
                throw new \InvalidArgumentException;
            }

            return $this->parsedValues[$matches[3]] ?? (string) getenv($matches[3]);
        }, $value);
    }

    private function handleCommands(string $value): string
    {
        return preg_replace_callback('#
          (\\\\)?
          \$
          (?<cmd>
            \(
            ([^()]|\g<cmd>)+
            \)
          )
        #x', function ($matches) {
            if ($matches[1] === '\\') {
                return substr($matches[0], 1);
            }

            // @codeCoverageIgnoreStart
            if (DIRECTORY_SEPARATOR === '\\') {
                throw new \LogicException('Resolving commands is not supported on Windows.');
            }

            if (!class_exists(Process::class)) {
                throw new \LogicException('Resolving commands requires the Symfony Process component.');
            }
            // @codeCoverageIgnoreEnd

            $process = new Process('echo ' . $matches[0]);
            $process->inheritEnvironmentVariables(true);
            $process->setEnv($this->parsedValues);
            try {
                $process->mustRun();
                // @codeCoverageIgnoreStart
            } catch (\Symfony\Component\Process\Exception\ExceptionInterface $e) {
                throw new \RuntimeException(sprintf(
                    'Error running command %s (%s)',
                    $matches[0],
                    $process->getErrorOutput()
                ));
                // @codeCoverageIgnoreEnd
            }

            return preg_replace('/[\r\n]+$/', '', $process->getOutput());
        }, $value);
    }
}
