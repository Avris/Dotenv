<?php
namespace Avris\Dotenv\Service;

final class Populator
{
    /** @var array */
    private static $populated = [];

    public function populate(iterable $vars): array
    {
        foreach ($vars as $name => $value) {
            if (is_iterable($value)) {
                $this->populate($value);
            } else {
                $this->putVar($name, $value);
            }
        }

        return self::$populated;
    }

    private function putVar($name, $value)
    {
        $notHttp = 0 !== strpos($name, 'HTTP_');
        if (!isset(self::$populated[$name]) && (isset($_ENV[$name]) || (isset($_SERVER[$name]) && $notHttp))) {
            return;
        }

        putenv("$name=$value");
        $_ENV[$name] = $value;
        if ($notHttp) {
            $_SERVER[$name] = $value;
        }

        self::$populated[$name] = $value;
    }
}
