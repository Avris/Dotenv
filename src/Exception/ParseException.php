<?php
namespace Avris\Dotenv\Exception;

use Avris\Dotenv\Line\Line;

class ParseException extends \Exception
{
    public function __construct(
        Line $line,
        string $message = 'Parsing error',
        int $code = 0,
        \Throwable $previous = null
    ) {
        parent::__construct(sprintf('%s in line %s', $message, $line->getNumber()), $code, $previous);
    }
}
