<?php
namespace Avris\Dotenv;

use Avris\Dotenv\Service as Service;

final class Dotenv
{
    /** @var Service\Parser */
    private $parser;

    /** @var Service\Populator */
    private $populator;

    /** @var Service\Filler */
    private $filler;

    /** @var Service\Dumper */
    private $dumper;

    /** @var Service\Filesystem */
    private $filesystem;

    public function __construct()
    {
        $this->parser = new Service\Parser;
        $this->populator = new Service\Populator;
        $this->filler = new Service\Filler;
        $this->dumper = new Service\Dumper;
        $this->filesystem = new Service\Filesystem;
    }

    public function load(string $filename): array
    {
        return $this->populator->populate($this->read($filename));
    }

    public function read(string $filename): array
    {
        return $this->parse($this->filesystem->readFile($filename));
    }

    public function parse(string $string): array
    {
        return $this->parser->parse($string);
    }

    public function populate(iterable $vars)
    {
        return $this->populator->populate($vars);
    }

    public function fill(string $filename, iterable $expected): iterable
    {
        return $this->filler->fill(
            $this->parser->getLines($this->filesystem->readFile($filename)),
            $expected
        );
    }

    public function dump(iterable $vars): string
    {
        return $this->dumper->dump($vars);
    }

    public function save(string $filename, $vars): string
    {
        return $this->filesystem->saveFile(
            $filename, // @codeCoverageIgnore
            is_iterable($vars) ? $this->dump($vars) : $vars
        );
    }
}
