<?php
namespace Avris\Dotenv\Line;

final class IgnoredLine extends Line
{
    /** @var string */
    private $content;

    public function __construct(string $content, int $number = -1)
    {
        $this->content = $content;
        parent::__construct($number);
    }

    public function getContent(): string
    {
        return $this->content;
    }
}
