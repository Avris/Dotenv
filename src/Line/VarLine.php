<?php
namespace Avris\Dotenv\Line;

final class VarLine extends Line
{
    /** @var string */
    private $name;

    /** @var string */
    private $value;

    public function __construct(string $name, string $value, int $number = -1)
    {
        $this->name = $name;
        $this->value = $value;
        parent::__construct($number);
    }

    public function getContent(): string
    {
        return sprintf(
            '%s=%s',
            $this->getName(),
            strpos($this->getValue(), ' ') === false
                ? $this->getValue()
                : '"' . str_replace('"', '\"', $this->getValue()) . '"'
        );
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
