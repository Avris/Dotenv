<?php
namespace Avris\Dotenv\Line;

abstract class Line
{
    /** @var int */
    private $number;

    public function __construct(int $number = -1)
    {
        $this->number = $number;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    abstract public function getContent(): string;

    public function __toString(): string
    {
        return $this->getContent() . "\n";
    }
}
