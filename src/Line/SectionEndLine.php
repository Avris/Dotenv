<?php
namespace Avris\Dotenv\Line;

final class SectionEndLine extends Line
{
    /** @var string */
    private $section;

    public function __construct(string $section, int $number = -1)
    {
        $this->section = $section;
        parent::__construct($number);
    }

    public function getContent(): string
    {
        return sprintf('###< %s ###', $this->getSection());
    }

    public function getSection(): string
    {
        return $this->section;
    }
}
