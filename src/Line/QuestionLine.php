<?php
namespace Avris\Dotenv\Line;

final class QuestionLine extends Line
{
    /** @var string */
    private $name;

    /** @var string */
    private $default;

    /** @var ?string */
    private $value;

    public function __construct(
        string $name,
        string $default
    ) {
        parent::__construct(-1);
        $this->name = $name;
        $this->default = $default;
    }

    public function getContent(): string
    {
        return sprintf(
            '%s=%s',
            $this->getName(),
            strpos($this->getValue(), ' ') === false
                ? $this->getValue()
                : '"' . str_replace('"', '\"', $this->getValue()) . '"'
        );
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDefault(): string
    {
        return $this->default;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function answer(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
